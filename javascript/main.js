
module.exports = function(e) {
    var t = {};
    function i(n) {
        if (t[n]) return t[n].exports;
        var r = t[n] = {
            i: n,
            l: !1,
            exports: {}
        };
        return e[n].call(r.exports, r, r.exports, i),
        r.l = !0,
        r.exports
    }
    return i.m = e,
    i.c = t,
    i.d = function(e, t, n) {
        i.o(e, t) || Object.defineProperty(e, t, {
            enumerable: !0,
            get: n
        })
    },
    i.r = function(e) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
            value: "Module"
        }),
        Object.defineProperty(e, "__esModule", {
            value: !0
        })
    },
    i.t = function(e, t) {
        if (1 & t && (e = i(e)), 8 & t) return e;
        if (4 & t && "object" == typeof e && e && e.__esModule) return e;
        var n = Object.create(null);
        if (i.r(n), Object.defineProperty(n, "default", {
            enumerable: !0,
            value: e
        }), 2 & t && "string" != typeof e) for (var r in e) i.d(n, r,
        function(t) {
            return e[t]
        }.bind(null, r));
        return n
    },
    i.n = function(e) {
        var t = e && e.__esModule ?
        function() {
            return e.
        default
        }:
        function() {
            return e
        };
        return i.d(t, "a", t),
        t
    },
    i.o = function(e, t) {
        return Object.prototype.hasOwnProperty.call(e, t)
    },
    i.p = "",
    i(i.s = 0)
} ([function(e, t, i) {
    "use strict";
    var n = i(1),
    r = n(i(2)),
    a = n(i(3)),
    o = n(i(4)),
    u = i(5),
    l = i(6),
    s = function() {
        function e(t, i) { (0, r.
        default)(this, e),
            this._runtime = t,
            this._extensionId = i,
            this._runtime.registerPeripheralExtension(this._extensionId, this),
            this._serialBuffer = Buffer.from([])
        }
        return (0, a.
    default)(e, [{
            key: "scan",
            value: function() {
                var e = this;
                console.log("scan"),
                this._intervalID && clearInterval(this._intervalID),
                this._timeoutID && clearTimeout(this._timeoutID);
                var t = this._runtime.getSerialport();
                this._intervalID = setInterval((function() {
                    t.list((function(t, i) {
                        var n;
                        t ? (e._runtime.changePeripheralStatus("PERIPHERAL_REQUEST_ERROR"), clearInterval(e._intervalID), e._intervalID = null, clearTimeout(e._timeoutID), e._timeoutID = null) : (n = i.map((function(e) {
                            if (0 !== e.pnpId.indexOf("USB") && 0 != e.pnpId.indexOf("FTDIBUS")) return null;
                            var t = "";
                            return - 1 !== e.pnpId.indexOf("VID_1A86&PID_7523") ? t = "-CH340": -1 !== e.pnpId.indexOf("VID_2341&PID_0043") ? t = "-Uno": -1 !== e.pnpId.indexOf("VID_0D28&PID_0204") ? t = "-Microbit": -1 !== e.pnpId.indexOf("VID_2341&PID_8036") ? t = "-Leonardo": -1 !== e.pnpId.indexOf("VID_2341&PID_0036") ? t = "-Leonardo-bootloader": -1 !== e.pnpId.indexOf("VID_0403+PID_6001") ? t = "-FT232": -1 !== e.pnpId.indexOf("VID_10C4&PID_EA60") ? t = "-CP210x": -1 !== e.pnpId.indexOf("VID_2341&PID_0042") && (t = "-Mega2560"),
                            {
                                name: "".concat(e.comName).concat(t),
                                peripheralId: e.comName
                            }
                        })).filter((function(e) {
                            return e
                        }))).length && (e._runtime.changePeripheralStatus("PERIPHERAL_LIST_UPDATE", n), clearInterval(e._intervalID), e._intervalID = null, clearTimeout(e._timeoutID), e._timeoutID = null)
                    }))
                }), 1e3),
                this._timeoutID = setTimeout((function() {
                    e._runtime.changePeripheralStatus("PERIPHERAL_SCAN_TIMEOUT"),
                    clearInterval(e._intervalID),
                    e._intervalID = null,
                    e._timeoutID = null
                }), 5e3)
            }
        },
        {
            key: "connect",
            value: function(e) {
                var t = this;
                console.log("connect");
                var i = this._runtime.getSerialport();
                this._serial = new i(e, {
                    baudRate: this._baudRate || 115200,
                    dataBits: 8,
                    parity: "none",
                    stopBits: 1,
                    flowControl: !1,
                    autoOpen: !0
                }),
                this._serial.on("open", (function() {
                    t._runtime.changePeripheralStatus("PERIPHERAL_CONNECTED")
                })),
                this._serial.on("error", (function() {
                    console.log("error"),
                    t._runtime.changePeripheralStatus("PERIPHERAL_DISCONNECTED"),
                    t._serial && t._serial.isOpen && t._serial.close(),
                    t._serial = null
                })),
                this._serial.on("close", (function() {
                    console.log("close"),
                    t._runtime.changePeripheralStatus("PERIPHERAL_DISCONNECTED"),
                    t._serial && t._serial.isOpen && t._serial.close(),
                    t._serial = null
                })),
                this._serial.on("data", (function(e) {
                    e = Buffer.from(e),
                    console.log("data", e.toString("hex")),
                    t._serialBuffer = Buffer.concat([t._serialBuffer, e]),
                    1024 < t._serialBuffer.length && (t._serialBuffer = t._serialBuffer.slice(t._serialBuffer.length - 1024))
                }))
            }
        },
        {
            key: "disconnect",
            value: function() {
                this._serial && this._serial.isOpen && this._serial.close(),
                this._serial = null,
                this._runtime.changePeripheralStatus("PERIPHERAL_DISCONNECTED")
            }
        },
        {
            key: "isConnected",
            value: function() {
                return this._serial && this._serial.isOpen
            }
        },
        {
            key: "isAvailable",
            value: function() {
                return this._serialBuffer && this._serialBuffer.length
            }
        },
        {
            key: "getSerialData",
            value: function() {
                var e = this._serialBuffer;
                return this._serialBuffer = Buffer.from([]),
                e
            }
        },
        {
            key: "compareData",
            value: function(e, t) {
                if (!this._serialBuffer.length) return ! 1;
                if ("original" === e) {
                    var i = Buffer.from(t.split(" ").filter((function(e) {
                        return e
                    })).map((function(e) {
                        return parseInt(e, 16)
                    }))),
                    n = i.compare(this._serialBuffer);
                    return console.log("original", i, this._serialBuffer, n),
                    -1 !== n && (this._serialBuffer = this._serialBuffer.slice(n + i.length)),
                    -1 !== n
                }
                var r = this._serialBuffer.toString().indexOf(t);
                return - 1 !== r && (this._serialBuffer = this._serialBuffer.slice(r + t.length)),
                -1 !== r
            }
        },
        {
            key: "write",
            value: function(e) {
                this._serial.write(Buffer.from(e))
            }
        },
        {
            key: "setBaudRate",
            value: function(e) {
                console.log("setBaudRate", e),
                this.isConnected ? this._serial.update({
                    baudRate: e
                }) : this._baudRate = e
            }
        },
        {
            key: "flowControl",
            value: function(e) {
                console.log("flowControl", e),
                this._serial.set(e)
            }
        }]),
        e
    } (),
    c = function() {
        function e(t, i) { (0, r.
        default)(this, e),
            this.runtime = t,
            console.log("EXTENSION_ID", i),
            this._peripheral = new s(this.runtime, i)
        }
        return (0, a.
    default)(e, [{
            key: "dispose",
            value: function() {
                this._peripheral.disconnect()
            }
        },
        {
            key: "getInfo",
            value: function() {
                return {
                    name: "实时飞行",
                    blockIconURI: o.default,
                    showStatusButton: !0,
					//blockIconHeight: 38,
                    blocks: [
					
					// {
                        // opcode: "whenReceivedMessage",
                        // blockType: l.HAT,
                        // text: "当接收到 [TYPE] [MSG]",
                        // arguments: {
                            // MSG: {
                                // type: u.STRING,
                                // defaultValue: "hello"
                            // },
                            // TYPE: {
                                // type: u.STRING,
                                // menu: "messageType",
                                // defaultValue: "text"
                            // }
                        // }
                    // },
                  {
                        opcode: "setBaudRate",
                        blockType: l.COMMAND,
                        text: "设置串口波特率 [BAUDRATE]",
                        arguments: {
                            BAUDRATE: {
                                type: u.NUMBER,
                                defaultValue: 115200
                            }
                        }
                    },					
                    // "---", {
                        // opcode: "sendData",
                        // blockType: l.COMMAND,
                        // text: "发送 [TYPE] [MSG]",
                        // arguments: {
                            // MSG: {
                                // type: u.STRING,
                                // defaultValue: "hello"
                            // },
                            // TYPE: {
                                // type: u.STRING,
                                // menu: "messageType",
                                // defaultValue: "text"
                            // }
                        // }
                    // },
                   "---",   {//初始化************************************************************************
                        opcode: "chushi",
                        blockType: l.COMMAND,
                        text: "无人机初始化"  
                    },	
                    {//起飞************************************************************************
                        opcode: "up",
                        blockType: l.COMMAND,
                        text: "起飞[distance]厘米",
                        arguments: {
                            distance: {
                                type: u.NUMBER,
                                defaultValue:100
                            }
                        }
                    },					
                    {//前后左右上下移动//************************************************************************
                        opcode: "move",
                        blockType: l.COMMAND,
                        text: "向 [direction] 移动[distance]厘米",
                        arguments: {
                            direction: {
                                type: u.STRING,
								menu: "direction",
                                defaultValue: "03"
                            },
                            distance: {
                                type: u.NUMBER,
                                defaultValue:0
                            }
                        }
                    },
					
                    {//斜线移动//************************************************************************
                        opcode: "move_FLY",
                        blockType: l.COMMAND,
						
                        text: "往[QH][qh_num]cm [ZY][zy_num]cm [SX][sx_num]cm",
                        arguments: {
                            QH: {type: u.STRING,menu: "QH",defaultValue: "01"},
							ZY: {type: u.STRING,menu: "ZY",defaultValue: "01"},
							SX: {type: u.STRING,menu: "SX",defaultValue: "01"},							
                            qh_num: {type: u.NUMBER,defaultValue:0},
							zy_num: {type: u.NUMBER,defaultValue:0},
							sx_num: {type: u.NUMBER,defaultValue:0}
                        }
                    },	
					
                    {//旋转//************************************************************************
                        opcode: "RotateDATA",
                        blockType: l.COMMAND,
                        text: "[Rotate]旋转[distance]度",
                        arguments: {
                            Rotate: {
                                type: u.STRING,
								menu: "Rotate",
                                defaultValue: "01"
                            },
                            distance: {
                                type: u.NUMBER,
                                defaultValue:0
                            }
                        }
                    },
                    {//灯光控制************************************************************************
                        opcode: "LEDCON",
                        blockType: l.COMMAND,
                        text: "灯光[clour]色[state]",
                        arguments: {
                            clour: {
                                type: u.STRING,
								menu: "clour",
                                defaultValue: "01"
                            },
                            state: {
                                type: u.STRING,
								menu: "state",
                                defaultValue: "00"
                            }
                        }
                    },					
                    {//降落************************************************************************
                        opcode: "land",
                        blockType: l.COMMAND,
                        text: "降落"  
                    },						
                    // {
                        // opcode: "isAvailable",
                        // blockType: l.BOOLEAN,
                        // text: "串口有数据可读"
                    // },
                    // {
                        // opcode: "readData",
                        // blockType: l.REPORTER,
                        // text: "读取 [TYPE]",
                        // arguments: {
                            // TYPE: {
                                // type: u.STRING,
                                // menu: "messageType",
                                // defaultValue: "text"
                            // }
                        // }
                    // },
  
                    // {
                        // opcode: "flowControl",
                        // blockType: l.COMMAND,
                        // text: "设置 DTR [DTR] RTS [RTS]",
                        // arguments: {
                            // DTR: {
                                // type: u.STRING,
                                // menu: "boolean",
                                // defaultValue: "True"
                            // },
                            // RTS: {
                                // type: u.STRING,
                                // menu: "boolean",
                                // defaultValue: "True"
                            // }
                        // }
                    // }
					],
                    menus: {
                        messageType: [{text: "原始数据", value: "original"},{text: "字符串",value: "text"}],
                        modeType: [{text: "初始化",value: "original"},{text: "起2",value: "text"}],						
                        boolean: ["True", "False"],
						direction:[{text: "上", value:"01"},{text: "下",value: "02"},{text: "前",value: "03"},{text: "后",value: "04"},{text: "左",value: "05"},{text: "右",value: "06"}],
						Rotate:[{text: "顺时针", value:"01"},{text: "逆时针",value: "02"}],
						clour:[{text: "黑", value:"01"},{text: "白",value: "02"},{text: "红",value: "03"},{text: "绿",value: "04"},{text: "蓝",value: "05"},{text: "紫",value: "06"},{text: "青",value: "07"},{text: "黄",value: "08"}],
						state:[{text: "常亮", value:"00"},{text: "七彩变换",value: "02"},{text: "呼吸灯",value: "01"}],
						QH:[{text: "前", value:"01"},{text: "后",value: "02"}],
						ZY:[{text: "左", value:"01"},{text: "右",value: "02"}],
						SX:[{text: "上", value:"01"},{text: "下",value: "02"}],
						
						
						
						//************************************************************************
                    }
                }
            }
        },
        {
            key: "whenReceivedMessage",
            value: function(e) {
                return ! ("string" != typeof e.TYPE || "string" != typeof e.MSG || !this._peripheral.isConnected()) && this._peripheral.compareData(e.TYPE, e.MSG)
            }
        },
        {
            key: "sendData",
            value: function(e) {
                var t;
                "string" == typeof e.TYPE && "string" == typeof e.MSG && this._peripheral.isConnected() && ("original" === e.TYPE ? (t = Buffer.from(e.MSG.split(" ").filter((function(e) {
                    return e
                })).map((function(e) {
                    return parseInt(e, 16)
                }))), this._peripheral.write(t)) : this._peripheral.write(e.MSG))
            }
        },
        {
            key: "isAvailable",
            value: function() {
                return !! this._peripheral.isConnected() && this._peripheral.isAvailable()
            }
        },
        {//初始化************************************************************************
          key: "chushi", value: function () {

              var msg="AA FA 21 00 00 00 00 00 00 00 00 00 FE"; 

              //*************************
              if(this._peripheral.isConnected()){
                  var t = Buffer.from(msg.split(" ").filter((function (e) {
                      return e
                  })).map((function (e) {
                      return parseInt(e, 16)
                  })))
                  this._peripheral.write(t);}
		       //*************************
          }          
        },
        {//起飞************************************************************************
          key: "up", value: function (e) {
              var distance = parseInt(e.distance).toString(16); 
              distance = ("0".repeat(4-distance.length)+distance).replace(/(.{2})/g, "$1 ").toUpperCase();
              var msg="AA FA 22 "+ distance +"00 00 00 00 00 00 00 FE"; 
              //*************************
              if(this._peripheral.isConnected()){
                  var t = Buffer.from(msg.split(" ").filter((function (e) {
                      return e
                  })).map((function (e) {
                      return parseInt(e, 16)
                  })))
                  this._peripheral.write(t);}
		       //*************************
    

          }          
        },			
        {//移动************************************************************************
          key: "move", value: function (e) {
              var distance = parseInt(e.distance).toString(16); 
              distance = ("0".repeat(4-distance.length)+distance).replace(/(.{2})/g, "$1 ").toUpperCase();
              var msg="AA FA 23 "+ e.direction + " "+ distance +"00 00 00 00 00 00 FE"; 
              //*************************
              if(this._peripheral.isConnected()){
                  var t = Buffer.from(msg.split(" ").filter((function (e) {
                      return e
                  })).map((function (e) {
                      return parseInt(e, 16)
                  })))
                  this._peripheral.write(t);}
		       //*************************
    

          }          
        },	
        {//斜线移动************************************************************************
          key: "move_FLY", value: function (e) {
			  
              var qh_num = parseInt(e.qh_num).toString(16); 
              qh_num = ("0".repeat(4-qh_num.length)+qh_num).replace(/(.{2})/g, "$1 ").toUpperCase();
			  
              var zy_num = parseInt(e.zy_num).toString(16); 
              zy_num = ("0".repeat(4-zy_num.length)+zy_num).replace(/(.{2})/g, "$1 ").toUpperCase();
			  
              var sx_num = parseInt(e.sx_num).toString(16); 
              sx_num = ("0".repeat(4-sx_num.length)+sx_num).replace(/(.{2})/g, "$1 ").toUpperCase();
			  
              var msg="AA FA 24 "+e.QH+ " "+ qh_num+ e.ZY+ " "+ zy_num + e.SX+ " "+ sx_num+"FE"; 
              //*************************
              if(this._peripheral.isConnected()){
                  var t = Buffer.from(msg.split(" ").filter((function (e) {
                      return e
                  })).map((function (e) {
                      return parseInt(e, 16)
                  })))
                  this._peripheral.write(t);}
		       //*************************
    

          }          
        },			
        {//旋转************************************************************************
            key: "RotateDATA",
            value: function(e) {
              var distance = parseInt(e.distance).toString(16); 
              distance = ("0".repeat(4-distance.length)+distance).replace(/(.{2})/g, "$1 ").toUpperCase();
              var msg="AA FA 25 "+ e.Rotate + " "+ distance +"00 00 00 00 00 00 FE"; 
              //*************************
              if(this._peripheral.isConnected()){
                  var t = Buffer.from(msg.split(" ").filter((function (e) {
                      return e
                  })).map((function (e) {
                      return parseInt(e, 16)
                  })))
                  this._peripheral.write(t);}
		       //*************************
            }
        },       
		{//灯光控制************************************************************************
            key: "LEDCON",
            value: function(e) {				
              var msg="AA FA 26 "+ e.clour + " "+ e.state + " "+"00 00 00 00 00 00 00 FE"; 
              //*************************
              if(this._peripheral.isConnected()){
                  var t = Buffer.from(msg.split(" ").filter((function (e) {
                      return e
                  })).map((function (e) {
                      return parseInt(e, 16)
                  })))
                  this._peripheral.write(t);}
		       //*************************
            }
        },
        {//降落************************************************************************
          key: "land", value: function () {

              var msg="AA FA 2f 00 00 00 00 00 00 00 00 00 FE"; 

              //*************************
              if(this._peripheral.isConnected()){
                  var t = Buffer.from(msg.split(" ").filter((function (e) {
                      return e
                  })).map((function (e) {
                      return parseInt(e, 16)
                  })))
                  this._peripheral.write(t);}
		       //*************************
          }          
        },


		
        {
            key: "readData",
            value: function(e) {
                if ("string" == typeof e.TYPE && this._peripheral.isConnected()) return "original" === e.TYPE ? this._peripheral.getSerialData().toString("hex").replace(/(.{2})/g, "$1 ").toUpperCase().slice(0, -1) : this._peripheral.getSerialData().toString()
            }
        },
        {
            key: "setBaudRate",
            value: function(e) {
                this._peripheral.setBaudRate(parseInt(e.BAUDRATE))
            }
        },
        {
            key: "flowControl",
            value: function(e) {
                this._peripheral.isConnected() && this._peripheral.flowControl({
                    dtr: "True" === e.DTR,
                    rts: "True" === e.RTS
                })
            }
        }]),
        e
    } ();
    e.exports = c
},
function(e, t) {
    e.exports = function(e) {
        return e && e.__esModule ? e: {
        default:
            e
        }
    }
},
function(e, t) {
    e.exports = function(e, t) {
        if (! (e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
},
function(e, t) {
    function i(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1,
            n.configurable = !0,
            "value" in n && (n.writable = !0),
            Object.defineProperty(e, n.key, n)
        }
    }
    e.exports = function(e, t, n) {
        return t && i(e.prototype, t),
        n && i(e, n),
        e
    }
},
function(e, t, i) {
    "use strict";
    i.r(t),
    t.
default ="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADEAAAArCAYAAADR0WDhAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAjVSURBVGhDzVkLbFPnFc7Wrl3Vdqyrtjbk4djXb997/X6/4jwcx3YeTgImOC+cJ4QgWNtBQFkg6UQQo2U8Nk1EAm1CqiKhRGVNx2NiIJF2Eisb2sY0LZOoVCGtakU3dS2jzb9zbn6PYK4dB5WQT/pk3+tzzv+f+5///Odc560kzM3NPdXX1z3k9bo/VStkRFq0mhj0HEkmkz+nIisX+/fvl0RDwWGbUX9drZB+wZQUErn0LlVyyZ3q6rKfUvGVhSNHjkiCFYGf6DntJ2qFhDAl+cCCexxAyiSriU6rIBu62jup6qNHd3c3U1HmPcpzGqJUFBNpyWrCwETFHEhRJsknbof1rxByT1Izjw6HDx9+PpFo3l1a6jrtcdve8PucJxwOy5TVoJvVqmQZHWHAUVanJENDg25qauUBnvDT9fU1mzVK5r+M5H4n5LBPNAopaY43fJ+qPHzgE09f+lOnTuXPzMw8RS9FEQqWjaiYkvudACqkxaSqwr98mWpwcJCPhKouJhLrak+cOFFQWekfa21dd/zChQvfpCKiOHTokJRnVZ9h+NznCIRamd/zFhVdHng89nGWVRKdTnnLZNSRgYGNfvpTRhBCHrOYuT+LOYH3fG7bZSq6PHjppS1WrVJGCgueJ16v4yq9vSisJv20Is0BgbASNqtxeZ3wOZ0njKyWGA26jwwGHenv762kP2WF2aB/UyEtEnfCxC+fE0NDQ9ZoNPJ2Mtlefvny5e+FQhW7YH+MT0xMPEFFMsJiNvwm3QEGKJW8SBxW0x+p2MPHsWPHXoD4/jq9FDAxcW4VbOzH6aUoQOcJk4GfZSQLz4sCcGA14Tg1iccbdlDRlQ27zXQ1dejhZlYyxcRpNf9tx44dTipyP/r7+7naaPhH4VBoJBKp3pNiDWU0HB6JhkM/LPX5Nvk9no2lPs+m3OjDzy3t7YnyK7NXVsH3120W06t2q3lPOi0mw2ikqmrP5OTkt+PxxmqdhiHS4hcJq5WTYLB8HFcI7DgCftdRsDEKHHZYzQfLS0t/Jjjhclp/q5ZLiRoOGbUcmPrMlSifgUqZhJj07O2zZ89ydpv5PN5TwT0Vcy/xaetUDFkTqxvFOXmc1rMWE/fvtsS6JryORIJbea1qTgkHHurLYePzrJpEw8F9eW1tiR+z4DUaEhyhRKPZCrIUUUZRUkQ0C3TTqQFHywK+X1+duVpg0Ko/vjfe7xKLPCjHb16/fv3Z7du3FMMJXwgr8IzP65rUqKRCaGGGYooLiJHXfZrsSibQwbzGxvq1wfJAsr4msjkWre6vnWdfY6xmCGqVO4s5opAVkcoy/1s1kVDPAv3+1Hf8rAuHBmJ10S4cLxaL9mrgiYvZxXsaaIY62tp6Uba3s9MNTt1QwhjowLxOEXHaLf8Y3rnTgTJZ0bp+fVSrZITCK32wFLF81rPqLyET8VQtJ8BJfkYhE7cpg6fstBp/H6uJ7OY0qi9TzmJPgaEYCHgv/BJWiJrKDpfN/LYcwiSbE2jY77HPUJWcMbprF8OzmluiqwzjYaGHE06FnRTCDFcvUh0cz7mfEAbRqT4Xq1/uEpZeWULa2lo2U7UlActuXGlRRxYQHeBgA8fja5Z2PoSDFdvVCiyHMw+Am9BkZP81NTW1mqotGT6HfQr3lJj9FPW87oPe3l4hQ+WM+SqS/0P2VcAlLyRVZf6TVO2BUFdTvUclLxa1j8QH2d6aeIWK546eng0enUYuZAMxw0gMARZktg1sClG1JaO6qnwsWzuKxN+sJsPspUuXvkvVckNZwHVUyWRfYswgDpvx77BqWRscMVy5Mruq1O+ZmH+7kX0/CGQKSShU/guqvjjOnTu3yqhnby4WStg+NjXUj1G1nLF3eFjmcdqvLDxI8TObM9KSfOF1zcDApjpqJjvWx5vWazMcRCliWuV16jsHDx5kqVpOSCaTPqOBe19odujEkQZO90mpz/073OCZxsUx4YC7ce3ateeouczwueynxYwsJA7kd7suUpWcEI83dfKc8rZMiitcMH9wwab1uRzvHj9+vATC8ms2i/46Oic2JuqoYPXqwuH5Yi8TRkdHpaxO9R9MnZkMoQMa2IwdHR1CWZALKivLd2vUjDBxGdgogfDgWdVcfTQ0gr/39fVVgxPf6IQSg9Mq57AKSI23cHxZMUSAVkVe3ro1KBgWA5Tcg6xOAZOUEswaYsSnB+Xvx+fPn3+BqmUEvrIJBEpPYyOjgk2sUUIxCHQ5LX8ZGNjoQpkyr/uQycCSrmSyG6/DoYp9mPVQ9r7xoVfHYtLncr5/8uRJ8bBqaGhQr13blGhvb+1ub2/pTGdHW6KrpaV5Q39/N0dVsmJ4ePg7NTXhlkSiuRcq5a62RKKroaE2gKGDVarXaZvGhyItzifw/U+4GsDHoJfwQhubxPFE5tFTF43Wj4+PP0uHeTTY2N3tspn0s/OV6XyI6iDc4FTuoCIrG7ASr0Bm+yI9C+FqOO3WG9B/P0NFVx5e27tX5nM7pjC2088gvMbMUxnw/erMmTNPU5WHg507d+bfvHlzyYM01tZ2G3n2w1Rjs9ABzFi4iSOh4MSyvLJvitUm1jbGxunlotjc2+vGPlkot/+fNu91APdCfW30EFV5+NiwocVuMfIkvqZxO70lis19fd6A3/0Gr1UKlW76X1fY9GDO51gVWbeucRtVWx5MT09/i9dq/ok9cKnX82br+njdvpERxYEDB+Qw8bJIddWQw25+RweTl4uEDlLY0JJCYjbyH/X0dDZQ08sLr8s+Of8HSJHwesXAqW4bOM3nnEYJB1EJkcHEkfjKMd0B7NBUcC54Pc53Xh8b01CTy4/m5rWNOHnhiQo5HiYNsZ2eMhcSf0PHWAifSE3Va3CgLbl8/0oBE3jc6bC8y2AdlVbb3EsMp/nY10L2cbts70Ep/cAN1FeObT/Yxhn07IdYzImtAN7Dl1ta2DtWE//BmsbYyyviH890DAz0Ge1280WNUvbZXQeKiEJWPKdVM7dsZv3MmljttveW2lY+EPLy/gdL6CR33nRfywAAAABJRU5ErkJggg=="
},
function(e, t, i) {
    "use strict";
    e.exports = {
        ANGLE: "angle",
        BOOLEAN: "Boolean",
        COLORPICKER: "colorpicker",
        COLORPALETTE: "colorpalette",
        SHOWLIGHTS: "showLights",
        RANGE: "range",
        NUMBER: "number",
        STRING: "string",
        INFRAREDTEXT: "infraredBtn",
        OBLOQPARAMETER: "obloq_initial_parameter",
        MQTTPARAMETER: "mqtt_setting_parameter",
        OBLOQHTTPPARAMETER: "obloq_initial_http_parameter",
        SETTINGS: "settings",
        TINYDBPARAMETER: "tinydb_settings_parameter",
        ESP32IMGSETTING: "esp32_img_settings",
        MPYSHOWIMG: "mpy_show_img",
        OLED2864IMGSETTING: "oled2864_img_settings",
        ESP32TEXTPREVIEW: "esp32_text_preview",
        OLED2864TEXTPREVIEW: "oled2864_text_preview",
        PIANO: "piano",
        STMOTORAXIS: "stepper_motor_axis_setting",
        PICTUREAIUSERCONFIG: "pictureai_userserver",
        PICTUREAIIMAGESETTING: "pictureai_img_setting",
        PICTUREAIDIRSETTING: "pictureai_dir_setting",
        PICTUREAIWEBIMGSETTING: "pictureai_webimg_setting",
        CAMERALIST: "cameralist_menu",
        MATRIX: "matrix",
        MATRIXICONS: "matrix_icons",
        NOTE: "note",
        CITYMENU: "city_menu",
        CONTENTINPUT: "content_input",
        NUMBERDROPDOWN: "number_dropdown",
        TEXTDROPDOWN: "text_dropdown"
    }
},
function(e, t, i) {
    "use strict";
    e.exports = {
        BOOLEAN: "Boolean",
        BUTTON: "button",
        COMMAND: "command",
        CONDITIONAL: "conditional",
        EVENT: "event",
        HAT: "hat",
        LOOP: "loop",
        REPORTER: "reporter"
    }
}]);